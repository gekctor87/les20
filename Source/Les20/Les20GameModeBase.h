// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Les20GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class LES20_API ALes20GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
